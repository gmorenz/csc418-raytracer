#include "ball.h"

using namespace glm;

Ball::Ball(GLFWwindow &window)
: window(window), type(Refracty2) {}

void Ball::reset() {
    printf("reset\n");
    squashMatrix = mat4();
    pos = vec3(0.0f, 0.0f, Z_PLANE);
    ballDir = normalize(vec3(drand48() - 0.5, drand48() - 0.5, 0));
    ball_move_speed = BALL_INITIAL_SPEED;
    next_type();
}

void Ball::next_type() {
    type = (BallType) ((type + 1) % MaxBallType);

    // Set ball radius
    switch (type) {
    case SolarBall:
        radius = 0.4;
        outer_radius = 0.4;
        break;
    case LightSource1:
        // Trying to minimize how "flashy" shadows are
        radius = 0.4;
        outer_radius = 0.5;
        break;
    default:
        radius = 0.2;
        outer_radius = 0.5;
        break;
    }

    // Print ball type
    switch (type) {
    case Refracty2:
        printf("Refracty2\n");
        break;
    case LightSource1:
        printf("LightSource1\n");
        break;
    case TexturedBall1:
        printf("TexturedBall1\n");
        break;
    case SolarBall:
        printf("Solar system\n");
        break;
    case MaxBallType:
        fprintf(stderr, "This should never happen 091354\n");
        exit(1);
    }
}

std::vector<Object> Ball::objects() {
    mat4 Tlight = glm::translate(mat4(), glm::vec3(0.0f, 1.0f, -9.5f));
    Object defaultLight = {
        glm::vec3(0.95f, 0.95f, 0.95f),
        0.0,
        { 0.0f, 0.0f, 0.0f, 0.0f },
        Tlight,
        inverse(Tlight),
        6.0f,
        0,
        1.0,
        OBJECT_LIGHT
    };

    switch (type) {
    case Refracty2: {
        mat4 T = translate(mat4(), pos);
        T = scale(T, vec3(outer_radius));
        T = T * squashMatrix;
        return {
            defaultLight,
            {
                vec3(0.0f, 0.0f, 0.0f),
                1.2,
                { 0.0f, 0.0f, 0.0f, 1.0f },
                T,
                inverse(T),
                1.0f,
                0.5f,
                .80,
                OBJECT_SPHERE | OBJECT_TRANSPARENT
            }
        };
    }
    case TexturedBall1: {
        mat4 T = translate(mat4(), pos);
        T = scale(T, vec3(outer_radius));

        float sign = ballDir.x < 0 ? -1.0f : 1.0f;
        float bound_distance = PADDLE_PLANE - radius;
        float rot = PI + sign * (pos.x - bound_distance) / (2 * bound_distance) * PI;
        T = rotate(T, rot, vec3(0, 1, 0));

        T = T * squashMatrix;
        return {
            defaultLight,
            {
                vec3(1.0f, 1.0f, 1.0f),
                1.2,
                { 0.3f, 0.75f, 0.15f, 0.0f },
                T,
                inverse(T),
                1.5f,
                1.0f,
                .80,
                OBJECT_SPHERE | OBJECT_TEXTURE
            }
        };
    }
    case LightSource1: {
        mat4 T = translate(mat4(), pos);
        T = scale(T, vec3(outer_radius));
        T = T * squashMatrix;
        return {{
            vec3(1.0f, 1.0f, 1.0f),
            1.2,
            { 0.5f, 0.95f, 0.35f, 0.35f },
            T,
            inverse(T),
            1.5f,
            0.5f,
            .80,
            OBJECT_SPHERE | OBJECT_LIGHT
        }};
    }
    case SolarBall: {
        mat4 Tinner = translate(mat4(), pos);
        Tinner = scale(Tinner, vec3(radius));
        float solar_outer = 1.15;

        float earth_radius = (solar_outer - 2 * radius) / 2;
        float sign = ballDir.x < 0 ? -1.0f : 1.0f;
        float bound_distance = PADDLE_PLANE - radius;
        float rot = PI + sign * (pos.x - bound_distance) / (2 * bound_distance) * 3 * PI;

        mat4 Touter = translate(mat4(), pos);
        Touter = rotate(Touter, rot, vec3(0, 1, 0));
        Touter = translate(Touter, vec3(solar_outer - earth_radius, 0.0f, 0.0f));
        Touter = scale(Touter, vec3(earth_radius));

        return {{
            vec3(1.0f, 1.0f, 0.5f),
            1.2,
            { 0.5f, 0.95f, 0.35f, 0.35f },
            Tinner,
            inverse(Tinner),
            2.5f,
            0.5f,
            1.0,
            OBJECT_SPHERE | OBJECT_LIGHT
        },
        {
            vec3(0.1f, 0.1f, 0.5f),
            1.0f,
            { 0.08f, 0.4f, 0.2f, 0.0f },
            Touter,
            inverse(Touter),
            0.0f,
            6.0f,
            0.0f,
            OBJECT_SPHERE | OBJECT_TEXTURE
        }};
    }
    case MaxBallType:
        fprintf(stderr, "This should never happen 091356\n");
        exit(1);
    }
    exit(1);
}

// returns true if game ended, otherwise false.
bool Ball::update(bool paused, double dt, float left_paddle, float right_paddle) {
    updateSpeed();
    if( paused ) { return false; }

    /* translate and clamp to play area */
    bool left = pos.x < 0;
    pos += vec3(squashMatrix * dt * ball_move_speed * vec4(ballDir, 0));
    if( left != (pos.x < 0) ) { next_type(); }
    squashMatrix = mat4();


    /* hit top or bottom of space */
    float ballSign = ballDir[1] / abs(ballDir[1]);
    if ((ballSign < 0 && pos.y - outer_radius <= LOWER_BOUND)
            || (ballSign > 0 && pos.y + outer_radius >= UPPER_BOUND)) {
        ballDir[1] *= -1;
    }

    if (fabsf(pos.x) > GOAL_PLANE) {
        return true;
    }

    if (pos.x <= -PADDLE_PLANE + outer_radius) {
        intersect(1.0, PADDLE_PLANE + pos.x, pos.y - left_paddle);
    } else if (pos.x >= PADDLE_PLANE - outer_radius) {
        intersect(-1.0, PADDLE_PLANE - pos.x, pos.y - right_paddle);
    }

    return false;
}

void Ball::updateSpeed() {
    if (glfwGetKey(&window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        ball_move_speed += BALL_MOVE_DELTA / 7.0;
    }
    if (glfwGetKey(&window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        ball_move_speed -= BALL_MOVE_DELTA / 7.0;
    }
}


// Handles intersection between paddle at y = 0, and
// ball distance x > 0 from paddle plane, height y
void Ball::intersect(float xSign, float ballX, float ballY) {
    float sign = ballY != 0 ? ballY / fabsf(ballY) : 1;
    bool collided = false;

    // Handle inner ball collision
    if (ballY < -PADDLE_HEIGHT - outer_radius
            || ballY > PADDLE_HEIGHT + outer_radius) {
        // we missed
    } else if ((ballY < -PADDLE_HEIGHT) || (ballY > PADDLE_HEIGHT)) {
        // We are above (or below) the paddle, but might be deflecting downwards.
        auto diffY = ballY - sign * PADDLE_HEIGHT;
        float distance = sqrt(ballX * ballX + diffY * diffY);

        collided = distance < radius;

        if (distance < outer_radius) {
            vec3 x_column = normalize(vec3(xSign * ballX, ballY - sign * PADDLE_HEIGHT, 0));
            vec3 y_column = vec3(0, 0, 1);
            vec3 z_column = cross(x_column, y_column);

            // Column major format
            float rot_arr[16] = {
                x_column.x, x_column.y, x_column.z, 0,
                y_column.x, y_column.y, y_column.z, 0,
                z_column.x, z_column.y, z_column.z, 0,
                0, 0, 0, 1
            };

            mat4 rot = make_mat4(rot_arr);
            float squash_factor = min(1.0f, abs(distance / outer_radius));
            squashMatrix = rot * scale(mat4(), vec3(squash_factor, 1.0f, 1.0f))
                * inverse(rot);
        }
    }
    else {
        collided = ballX < radius;
        float squash_factor = min(1.0f, abs(ballX / outer_radius));
        squashMatrix = glm::scale(mat4(), vec3(squash_factor, 1.0f, 1.0f));
    }

    collided = collided && (ballX > -PADDLE_DEPTH);

    if (collided) {
        ballDir = normalize(vec3(xSign * cos(5 * PI * ballY / 12),
                    sin(5 * PI * ballY / 12), 0));
        ball_move_speed += BALL_MOVE_DELTA;
        ball_move_speed = min(ball_move_speed, BALL_MAX_SPEED);
    }
}

/*
void Ball::scale(vec3 v) {
    auto trans = translate(vec3(ball.T * vec4(0, 0, 0, 1)));
    ball.T = ball.T * inverse(trans);
    ball.T = scale(ball.T, v);
    ball.T = ball.T * trans;
    ball.Tinv = inverse(ball.T);
}
*/
