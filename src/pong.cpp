#include "pong.h"

using namespace glm;

/* ugly work around to have c++ class interact with c api */
static void *globalPong;

/* external API */
void Pong::initScene(GLFWwindow *window) {
    globalPong = this;
    glfwSetKeyCallback(window, pongControlCallback);
}

void Pong::updateScene(double dt) {
        // Update ball, if someone won recreate the game.
    auto left_paddle = left[2].T * vec4(0, 0, 0, 1);
    auto right_paddle = right[2].T * vec4(0, 0, 0, 1);
    if( ball.update(paused, dt, left_paddle.y, right_paddle.y) ) {
        createGame();
    }

    updatePaddles(dt);
    updateBuffer();
}

std::vector<std::pair<uint32_t, const char *>> Pong::getTextures() {
    std::vector<std::pair<uint32_t, const char *>> textures = {
        {3, "../1_earth_8k.ppm"},
        {4, "../checkerboard.ppm"}
    };
    return textures;
}

/* internal functionality */

void pongControlCallback(GLFWwindow *window, int key, int scancode, int action,
        int mods) {
    return ((Pong *) globalPong)->controlCallback(window, key, scancode,
            action, mods);
}

void Pong::controlCallback(GLFWwindow *window, int key, int scancode, int action,
        int mods) {
    if (action == GLFW_PRESS) {
        if( key == GLFW_KEY_SPACE ) {
            if (paused) {
                paused = false;
            } else {
                createGame();
            }
        }
    }
}

Pong::Pong(GLFWwindow &window, struct rayCamera &camera, std::vector<Object> *objects)
    : window(window) {
    this->buffer = objects;

    /* initialize camera position */
    glm::vec3 eyePos = glm::vec3(0.0, 0.0, -6.0);
    glm::mat4 camera2world = glm::inverse(glm::lookAt(
                eyePos,
                eyePos + glm::vec3(0.0, 0.0, 1.0),
                glm::vec3(0.0, 1.0, 0.0)
                ));
    camera = { camera2world, eyePos, 1.5 };

    lense = {
        glm::vec3(0.0, 0.0, 0.0),
        1.3,
        { 0.0, 0.0, 0.0, 1.0 },
        glm::mat4(),
        glm::mat4(),
        0.0,
        6,
        0.0,
        OBJECT_SPHERE | OBJECT_TRANSPARENT
    };

    lense.T = camera2world * translate(mat4(), vec3(0.0, 0.0, -0.0201)) * scale(mat4(), vec3(1, 1, 0.002));
    lense.Tinv = inverse(lense.T);

    createGame();
}

void Pong::createGame() {
    createLights();
    ball.reset();
    createPaddle(left, -1.0f);
    createPaddle(right, 1.0f);
    createSurroundings();

    paused = true;
}

void Pong::createLights() {
    lights.clear();
    Object light = {
        glm::vec3(0.95f, 0.95f, 0.95f),
        0.0,
        { 0.0f, 0.0f, 0.0f, 0.0f },
        glm::mat4(),
        glm::mat4(),
        .10f,
        0,
        0.0,
        OBJECT_LIGHT
    };
    light.T = glm::translate(light.T, glm::vec3(0.0f, 2.5f, -9.5f));
    light.Tinv = glm::inverse(light.T);
    /* lights.push_back(light); */
}

void Pong::createPaddle(std::array<Object, 4> &paddle, float reflect) {
    for (auto i = 0; i < 6; i++) {
        paddle[i] = {
            vec3(0.0f, 0.0f, 1.0f),
            1.0,
            { 0.5f, 0.5f, 0.35f, 0.1f },
            mat4(),
            mat4(),
            0.0f,
            0.5f,
            0.0,
            OBJECT_PLANE
        };
    }

    /* top */
    createPlane(paddle[0].T, paddle[0].Tinv,
            vec3((reflect * (PADDLE_PLANE + (PADDLE_WIDTH / 2))),
                PADDLE_HEIGHT, Z_PLANE),
            (float) (3.141592 / 2), vec3(1.0f, 0.0f, 0.0f),
            vec3(PADDLE_WIDTH / 2, PADDLE_DEPTH, 1.0f));
    /* bottom */
    createPlane(paddle[1].T, paddle[1].Tinv,
            vec3(reflect * (PADDLE_PLANE + (PADDLE_WIDTH / 2)),
                -PADDLE_HEIGHT, Z_PLANE),
            (float) (3.141592 / 2), vec3(1.0f, 0.0f, 0.0f),
            vec3(PADDLE_WIDTH / 2, PADDLE_DEPTH, 1.0f));


    /* inner */
    createPlane(paddle[2].T, paddle[2].Tinv,
            vec3(reflect * PADDLE_PLANE, 0.0f, Z_PLANE),
            (float) -(3.141592 / 2),vec3(0.0f, 1.0f, 0.0f),
            vec3(PADDLE_DEPTH, PADDLE_HEIGHT, 1.0));

    /* front */
    createPlane(paddle[3].T, paddle[3].Tinv,
            vec3(reflect * (PADDLE_PLANE + (PADDLE_WIDTH / 2)), 0.0f,
                (Z_PLANE) - PADDLE_DEPTH),
            (float) 0, vec3(1.0f),
            vec3(PADDLE_WIDTH / 2, PADDLE_HEIGHT, 1.0f));
}

void Pong::createSurroundings() {
    for (auto &plane : surroundings) {
        plane = {
            vec3(0, 1, 0),
            0.0,
            { 0.5f, 0.95f, 0.35f, 0.25f + 0.75f * (float) drand48() },
            mat4(),
            mat4(),
            0.0f,
            1.0f,
            0.0,
            0
        };
    }

    /* back */
    surroundings[0].type = OBJECT_PLANE;
    createPlane(surroundings[0].T, surroundings[0].Tinv,
            vec3(0.0f, 0.0f, SCENE_DEPTH),
            (float) 0, vec3(1.0f, 0.0f, 0.0f),
            vec3(2 * PADDLE_PLANE, SCENE_HEIGHT, 1.0f));

    /* top */
    surroundings[1].type = OBJECT_PLANE;
    surroundings[1].color = vec3(1, 0, 1);
    createPlane(surroundings[1].T, surroundings[1].Tinv,
            vec3(0, UPPER_BOUND, Z_PLANE),
            (float) (3.141592 / 2), vec3(1.0f, 0.0f, 0.0f),
            vec3(2 * PADDLE_PLANE, SCENE_DEPTH, 1.0f));

    /* bottom */
    surroundings[2].type = OBJECT_PLANE;
    surroundings[2].color = vec3(1, 0, 1);
    createPlane(surroundings[2].T, surroundings[2].Tinv,
            vec3(0, -UPPER_BOUND, Z_PLANE),
            (float) (3.141592 / 2), vec3(1.0f, 0.0f, 0.0f),
            vec3(2 * PADDLE_PLANE, SCENE_DEPTH, 1.0f));

    /* right */
    surroundings[3].type = OBJECT_PLANE;
    createPlane(surroundings[3].T, surroundings[3].Tinv,
            vec3(PADDLE_PLANE + 3, 0.0f, Z_PLANE),
            (float) -(3.141592 / 2),vec3(0.0f, 1.0f, 0.0f),
            vec3(SCENE_DEPTH, 2 * UPPER_BOUND, 1.0));

    /* left */
    surroundings[4].type = OBJECT_PLANE;
    createPlane(surroundings[4].T, surroundings[4].Tinv,
            vec3(-PADDLE_PLANE - 3, 0.0f, Z_PLANE),
            (float) -(3.141592 / 2),vec3(0.0f, 1.0f, 0.0f),
            vec3(SCENE_DEPTH, 2 * UPPER_BOUND, 1.0));

    /* right */
    createPlane(surroundings[5].T, surroundings[5].Tinv,
            vec3(SCENE_WIDTH, 0.0f, 0.0f),
            (float) (PI / 2), vec3(0.0f, 1.0f, 0.0f),
            vec3(1.0f, SCENE_HEIGHT, SCENE_DEPTH));
}

void Pong::updatePaddles(double dt) {
    if (glfwGetKey(&window, GLFW_KEY_S) == GLFW_PRESS) {
        updatePaddle(left, DOWN, dt);
    }
    if (glfwGetKey(&window, GLFW_KEY_D) == GLFW_PRESS
            || glfwGetKey(&window, GLFW_KEY_W) == GLFW_PRESS) {
        updatePaddle(left, UP, dt);
    }
    if (glfwGetKey(&window, GLFW_KEY_J) == GLFW_PRESS
            || glfwGetKey(&window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        updatePaddle(right, DOWN, dt);
    }
    if (glfwGetKey(&window, GLFW_KEY_K) == GLFW_PRESS
            || glfwGetKey(&window, GLFW_KEY_UP) == GLFW_PRESS) {
        updatePaddle(right, UP, dt);
    }
}

void Pong::updatePaddle(std::array<Object, 4> &paddle, bool move, double dt) {
    /* position of inner paddle */
    auto pos = paddle[2].T * vec4(0, 0, 0, 1);
    auto translation = vec3(0.0, dt * PADDLE_MOVE_SPEED, 0.0f);

    if (move) {
        if (pos.y - PADDLE_HEIGHT / 2 <= LOWER_BOUND) {
            return;
        }
        translation *= -1;
    }
    else if (pos.y + PADDLE_HEIGHT / 2 >= UPPER_BOUND) {
        return;
    }

    for (auto &plane : paddle) {
        plane.T = translate(translation) * plane.T;
        plane.Tinv = inverse(plane.T);
    }
}

void Pong::updateBuffer() {
    auto i = 0;

    memset(buffer->data(), 0, buffer->size() * sizeof(Object));

    /* push lights into buffer */
    /* for (auto light : lights) { */
    /*     if (light.type != 0) (*buffer)[i++] = light; */
    /* } */

    for (auto ball_obj : ball.objects()) {
        if (ball_obj.type != 0) (*buffer)[i++] = ball_obj;
    }

/*
    if (lense.type != 0 ) {
         (*buffer)[i++] = lense;
    }
*/

    /* update paddles */
    for (auto &plane : left) {
        if (plane.type != 0) (*buffer)[i++] = plane;
    }

    for (auto &plane : right) {
        if (plane.type != 0) (*buffer)[i++] = plane;
    }

    for (auto &plane : surroundings) {
        if (plane.type != 0) (*buffer)[i++] = plane;
    }
}

