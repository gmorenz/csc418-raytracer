#ifndef TEXTURE_H
#define TEXTURE_H

#include "raytracer.h"
/* #include <gli/texture2d.hpp> */

class Texture {
public:
    VkDescriptorImageInfo descriptor;
    VDeleter<VkImage> image;

private:
    VDeleter<VkDevice> &device;
    VDeleter<VkImageView> view;
    VDeleter<VkSampler> sampler;
    VDeleter<VkDeviceMemory> memory;
    uint32_t binding;
    VkDescriptorType type;

public:
    /* compute storage texture */
    Texture(VDeleter<VkDevice> &device, VkPhysicalDevice physicalDevice,
            VkCommandPool commandPool, VkQueue queue, uint32_t width,
            uint32_t height, uint32_t binding, VkFormat format);
    /* sampled texture */
    Texture(VDeleter<VkDevice> &device, VkPhysicalDevice physicalDevice,
            VkCommandPool commandPool, VkQueue queue, const char *fName,
            uint32_t binding, VkFormat);
    VkDescriptorSetLayoutBinding getDescriptorSetLayoutBinding();
    VkWriteDescriptorSet getWriteDescriptorSet(VkDescriptorSet dstSet);

private:
    void checkSupport(VkPhysicalDevice physicalDevice, VkFormat format);
    void initDestructors();
    void createInfo(uint32_t width, uint32_t height, VkFormat format,
            VkImageTiling tiling);
    VkDeviceSize allocateMemory(VkPhysicalDevice physicalDevice);
    void populateMemory(gli::texture2d tex2D, VkDeviceSize size);
    VkCommandBuffer startCommand(VkCommandPool commandPool);
    void endCommand(VkCommandBuffer cmd, VkCommandPool, VkQueue queue);
    void createImageBarrier(VkCommandPool commandPool, VkQueue queue,
            VkImageLayout newImageLayout);
    void createSampler();
    void createImageView(VkFormat format);

    /* TODO remove duplicate in raytracer and factor into utils */
    static uint32_t findMemoryType(VkPhysicalDevice physicalDevice,
            uint32_t typeFilter, VkMemoryPropertyFlags props);
    void setImageLayout(VkCommandBuffer cmdbuffer, VkImage image,
        VkImageAspectFlags aspectMask, VkImageLayout oldImageLayout,
        VkImageLayout newImageLayout, VkImageSubresourceRange subresourceRange,
        VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
};

#endif
