#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <utility>

class Scene {
public:
    virtual void initScene(GLFWwindow *window) = 0;
    virtual void updateScene(double dt) = 0;
    virtual std::vector<std::pair<uint32_t, const char *>> getTextures() = 0;
};

#endif
