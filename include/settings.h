#ifndef SETTINGS_H
#define SETTINGS_H

// Have to keep in line with fragment shader!
static const int ANTI_ALIASING = 1;

static const float SCENE_DEPTH = 32.0f;
static const float SCENE_HEIGHT = 8.0f;
static const float SCENE_WIDTH = 12.0f;

static const float Z_PLANE = 0;

/* paddle size */
static const float PADDLE_PLANE = 4.0f;
static const float PADDLE_WIDTH = 0.15f;
static const float PADDLE_HEIGHT = 0.60f;
static const float PADDLE_DEPTH = 0.4f;

/* paddle movement */
static const float PADDLE_MOVE_SPEED = 0.005f;
static const float UPPER_BOUND = 2.75f;
static const float LOWER_BOUND = -2.75;
static const bool UP = false;
static const bool DOWN = true;

static const float GOAL_PLANE = PADDLE_PLANE + 1;

/* Ball constants */
static const float BALL_INITIAL_SPEED = 0.0040f;
static const float BALL_MOVE_DELTA = 0.00024f;
static const float BALL_MAX_SPEED = 0.090f;

#endif
