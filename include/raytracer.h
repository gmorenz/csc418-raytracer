#ifndef RAY_TRACER_H
#define RAY_TRACER_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <gli/gli.hpp>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cstdio>
#include <cstring>
#include <vector>
#include <utility>
#include <chrono>
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstring>
#include <set>

#include "vdeleter.h"

#define MAX_OBJECTS 100
#define PI 3.1415926f

#define OBJECT_SPHERE 0x01
#define OBJECT_PLANE 0x02
#define OBJECT_LIGHT 0x04
#define OBJECT_TRANSPARENT 0x20
#define OBJECT_TEXTURE 0x40

VkResult CreateDebugReportCallbackEXT(VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDebugReportCallbackEXT* pCallback);

void DestroyDebugReportCallbackEXT(VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks* pAllocator);

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objType,
        uint64_t obj, size_t location, int32_t code,
        const char* layerPrefix, const char* msg, void* userData);

struct QueueFamilyIndices {
    int graphics = -1;
    int present = -1;
    int compute = -1;

    bool isComplete() {
        return compute >= 0 && present >= 0 && graphics >= 0;
    }
};

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;

    bool isAdequate() {
        return !formats.empty() && !presentModes.empty();
    }
};

struct Buffer {
    VDeleter<VkBuffer> buffer;
    VDeleter<VkDeviceMemory> memory;
    VkDescriptorBufferInfo descriptor;
    VkDeviceSize size = 0;
    VkDeviceSize alignment = 0;
    void* mapped = nullptr;

    VkBufferUsageFlags usageFlags;
    VkMemoryPropertyFlags memoryPropertyFlags;
};

struct rayCamera {
    glm::mat4 c2w;
    glm::vec3 eyePos;
    float width;
};

struct Phong {
    float ambient_albedo;
    float diffuse_albedo;
    float specular_albedo;
    float global_albedo;
};

/* shared properties of an object */
struct Object {
    glm::vec3 color;
    float refraction_coefficient;
    struct Phong phong;

    /* object to world */
    glm::mat4 T;
    /* world to object */
    glm::mat4 Tinv;

    // Emits light color * emittance.
    float emittance;
    float shinyness;
    float light_dropoff;
    uint32_t type;
};

void createPlane(glm::mat4 &T, glm::mat4 &Tinv, glm::vec3 trans, float rad,
        glm::vec3 axis, glm::vec3 scaleV);

#endif
