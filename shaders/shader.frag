#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// #define ANTI_ALIASING

layout (binding = 0) uniform sampler2D samplerColor;

layout(origin_upper_left) in vec4 gl_FragCoord;

layout (location = 0) out vec4 outFragColor;

void main() {
#ifdef ANTI_ALIASING
    ivec2 coord = 2 * ivec2((gl_FragCoord.xy));
    vec4 c1 =  texelFetch(samplerColor, coord, 0);
    vec4 c2 =  texelFetch(samplerColor, coord + ivec2(1, 0), 0);
    vec4 c3=  texelFetch(samplerColor, coord + ivec2(1, 1), 0);
    vec4 c4=  texelFetch(samplerColor, coord + ivec2(0, 1), 0);

    outFragColor = (c1 + c2 + c3 + c4)  /  4.0;
#else
    outFragColor = texelFetch(samplerColor, ivec2(gl_FragCoord.xy), 0);
#endif
}
